from django.contrib import admin

from electronic_queue.doctor.models import Doctor


@admin.register(Doctor)
class DoctorAdmin(admin.ModelAdmin):
    search_fields = (
        'name',
    )
    list_display = (
        'name',
        'user',
    )
