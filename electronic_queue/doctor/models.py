from calendar import monthrange
from datetime import datetime, timedelta

from django.db import models

from electronic_queue.appointment.choices import AppointmentStatus
from electronic_queue.core.models import BaseModel


class Doctor(BaseModel):
    user = models.OneToOneField('account.User', on_delete=models.SET_NULL, blank=True, null=True, related_name='doctor')
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    @property
    def schedule_times(self):
        start = datetime(year=2020, month=1, day=1, hour=9, minute=0)
        return [(start + timedelta(minutes=i * 30)).strftime('%H:%M') for i in range(18)]

    def available_times(self, d):
        # now = datetime.now()
        # date = datetime.strptime(d, '%Y-%m-%d')
        reserved_times = self.appointments.filter(
            reservation_date__date=d,
            status__in=[AppointmentStatus.CREATED, AppointmentStatus.ACCEPTED]
        ).values_list('reservation_date__time', flat=True)
        reserved_times = [t.strftime('%H:%M') for t in reserved_times]
        times = [t if t not in reserved_times else None for t in self.schedule_times]
        return times

    def available_days(self, d):
        now = datetime.now()
        date = datetime.strptime(d, '%Y-%m-%d')
        day_count = monthrange(date.year, date.month)[1]
        days = [i for i in range(1, day_count + 1)]
        if now.month == date.month and now.year == date.year:
            days = [i for i in days if now.day <= i]
        return days
