from django.contrib import admin

from electronic_queue.appointment.models import Appointment
from electronic_queue.appointment.choices import AppointmentStatus


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = (
        'bot_user',
        'reservation_date',
        'doctor',
        'get_status_display',
    )

    def get_queryset(self, request):
        qs = super(AppointmentAdmin, self).get_queryset(request)
        user = request.user
        if not user.is_superuser:
            qs = qs.filter(doctor__user=request.user)
        return qs

    def get_status_display(self, obj):
        return dict(AppointmentStatus.CHOICES)[int(obj.status)]
