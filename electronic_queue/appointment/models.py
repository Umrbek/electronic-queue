from django.db import models

from electronic_queue.appointment.choices import AppointmentStatus
from electronic_queue.core.models import BaseModel


class Appointment(BaseModel):
    bot_user = models.ForeignKey('account.BotUser', on_delete=models.CASCADE, related_name='appointments')
    doctor = models.ForeignKey('doctor.Doctor', on_delete=models.CASCADE, related_name='appointments')
    status = models.CharField(max_length=12, choices=AppointmentStatus.CHOICES, default=AppointmentStatus.CREATED)
    reservation_date = models.DateTimeField()

    class Meta:
        ordering = (
            '-reservation_date',
        )
