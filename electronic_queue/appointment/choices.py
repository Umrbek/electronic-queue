

class AppointmentStatus:
    CREATED = 0
    ACCEPTED = 1
    REJECT = 2

    CHOICES = (
        (CREATED, 'CREATED'),
        (ACCEPTED, 'ACCEPTED'),
        (REJECT, 'REJECT')
    )

