from django.contrib import admin
from django import forms


from electronic_queue.account.models import User, BotUser


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = '__all__'

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        password = self.cleaned_data["password"]
        user.set_password(password)
        if commit:
            user.save()
        return user


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    form = UserForm


@admin.register(BotUser)
class BotUserAdmin(admin.ModelAdmin):
    search_fields = (
        'chat_id',
    )
    list_display = (
        'chat_id',
        'name',
        'user',
        'is_active',
        'updated_at',
        'created_at',
    )
    readonly_fields = (
        'created_at',
        'updated_at',
    )
    list_filter = (
        'is_active',
    )
