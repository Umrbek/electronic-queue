from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models

from electronic_queue.core.models import BaseModel


class User(AbstractUser):
    pass


class BotUser(BaseModel):
    user = models.OneToOneField(User, related_name='bot_user', blank=True, null=True, on_delete=models.SET_NULL)
    chat_id = models.CharField(max_length=128, unique=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    age = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.chat_id} ({self.name})'

    @property
    def info_text(self):
        text = f'Ф.И.О: {self.name}\n' \
               f'Возраст: {self.age}'
        return text
