import json
from datetime import datetime, timedelta

from telegram import Update
from telegram.ext import CallbackContext, CallbackQueryHandler

from bot.constants import AppointmentAction, TextMessage
from bot.keyboards import calendar_keyboard, time_keyboard, appointment_result_inline_menu
from electronic_queue.account.models import BotUser
from electronic_queue.appointment.choices import AppointmentStatus
from electronic_queue.appointment.models import Appointment
from electronic_queue.core.utils.etc import get_or_none
from electronic_queue.doctor.models import Doctor


class AppointmentCallback:

    @staticmethod
    def doctor_action_pattern():
        return f'^{{"id": \d+, "action": {AppointmentAction.DOCTOR.value}}}$'

    @staticmethod
    def doctor_choice(update: Update, context: CallbackContext):
        AppointmentCallback.calendar(update, context)

    @staticmethod
    def calendar(update: Update, context: CallbackContext, calendar_date=None):
        query = update.callback_query
        query.answer()
        data = json.loads(query.data)
        callback_format = '"id": {id}, "action": {action}, "date": "{date}"'
        data_format = str_to_json_format(
            callback_format.format(
                id=data['id'],
                date='{}',
                action=AppointmentAction.DATE.value
            ))
        prev_format = str_to_json_format(
            callback_format.format(
                id=data['id'],
                date='{}',
                action=AppointmentAction.DATE_PREV.value
            ))
        next_format = str_to_json_format(
            callback_format.format(
                id=data['id'],
                date='{}',
                action=AppointmentAction.DATE_NEXT.value
            ))

        doctor = get_or_none(Doctor, id=data['id'])
        if calendar_date is None:
            calendar_date = datetime.now().strftime('%Y-%m-%d')
            kb = calendar_keyboard(
                doctor.available_days(calendar_date),
                callback_data_format=data_format,
                prev_format=prev_format,
                next_format=next_format
            )
            context.bot.edit_message_text(
                text='Выберите дату',
                message_id=query.message.message_id,
                chat_id=query.message.chat_id,
                reply_markup=kb
            )
        else:
            _date = datetime.strptime(calendar_date, "%Y-%m-%d")
            kb = calendar_keyboard(
                doctor.available_days(calendar_date),
                year=_date.year,
                month=_date.month,
                callback_data_format=data_format,
                prev_format=prev_format,
                next_format=next_format
            )
            context.bot.edit_message_reply_markup(
                message_id=query.message.message_id,
                chat_id=query.message.chat_id,
                reply_markup=kb
            )

    @staticmethod
    def prev_date_action_pattern():
        return f'^{{"id": \d+, "action": {AppointmentAction.DATE_PREV.value}, "date": .+}}$'

    @staticmethod
    def prev_date(update: Update, context: CallbackContext):
        query = update.callback_query
        query.answer()
        data = json.loads(query.data)
        _date = datetime.strptime(data['date'], "%Y-%m-%d")
        now = datetime.now()
        if now.month < _date.month and now.year < _date.year or (now.year == _date.year and now.month < _date.month):
            calendar_date = (_date - timedelta(days=1)).strftime("%Y-%m-%d")
            AppointmentCallback.calendar(update, context, calendar_date=calendar_date)
        else:
            context.bot.delete_message(
                message_id=query.message.message_id,
                chat_id=query.message.chat_id
            )

    @staticmethod
    def next_date_action_pattern():
        return f'^{{"id": \d+, "action": {AppointmentAction.DATE_NEXT.value}, "date": .+}}$'

    @staticmethod
    def next_date(update: Update, context: CallbackContext):
        query = update.callback_query
        query.answer()
        data = json.loads(query.data)
        _date = datetime.strptime(data['date'], "%Y-%m-%d")
        calendar_date = (_date + timedelta(days=32)).strftime("%Y-%m-%d")
        AppointmentCallback.calendar(update, context, calendar_date=calendar_date)

    @staticmethod
    def date_action_pattern():
        return f'^{{"id": \d+, "action": {AppointmentAction.DATE.value}, "date": .+}}$'

    @staticmethod
    def date_choice(update: Update, context: CallbackContext):
        query = update.callback_query
        query.answer()
        data = json.loads(query.data)
        choice_date = data['date']
        callback_format = '"id": {id}, "action": {action}, "date": "{date}", "time": "{time}"'
        data_format = str_to_json_format(
            callback_format.format(
                id=data['id'],
                date=choice_date,
                time='{}',
                action=AppointmentAction.TIME.value
            ))
        doctor = get_or_none(Doctor, id=data['id'])
        if not doctor or not doctor.user or not doctor.user.bot_user:
            context.bot.delete_message(
                message_id=query.message.message_id,
                chat_id=query.message.chat_id
            )
            return
        kb = time_keyboard(
            doctor.available_times(choice_date),
            callback_data_format=data_format
        )
        context.bot.edit_message_text(
            text='Выберите время',
            message_id=query.message.message_id,
            chat_id=query.message.chat_id,
            reply_markup=kb
        )

    @staticmethod
    def time_action_pattern():
        return f'^{{"id": \d+, "action": {AppointmentAction.TIME.value}, "date": .+, "time": .+}}$'

    @staticmethod
    def time_choice(update: Update, context: CallbackContext):
        query = update.callback_query
        data = json.loads(query.data)
        bot_user = BotUser.objects.get(chat_id=query.message.chat_id)
        doctor = get_or_none(Doctor, id=data['id'])
        reservation_date = f'{data["date"]}T{data["time"]}'
        date_is_already_booked = Appointment.objects.filter(
                                        doctor=doctor,
                                        reservation_date=reservation_date,
                                        status__in=[AppointmentStatus.CREATED, AppointmentStatus.ACCEPTED]).exists()
        if not doctor or not doctor.user or not doctor.user.bot_user or date_is_already_booked:
            context.bot.delete_message(
                message_id=query.message.message_id,
                chat_id=query.message.chat_id
            )
            return
        appointment = Appointment.objects.create(
            doctor=doctor,
            bot_user=bot_user,
            reservation_date=reservation_date,
        )
        msg = TextMessage.WAIT_FOR_ACCEPT + f'\nДата: {data["date"]}\nВремя: {data["time"]}'
        context.bot.edit_message_text(
            text=msg,
            message_id=query.message.message_id,
            chat_id=query.message.chat_id,
            reply_markup=None
        )
        kb = appointment_result_inline_menu(appointment.id, data["date"], data["time"])
        context.bot.send_message(
            text=msg,
            chat_id=doctor.user.bot_user.chat_id,
            reply_markup=kb
        )

    @staticmethod
    def appointment_result(update: Update, context: CallbackContext, status=None):
        query = update.callback_query
        data = json.loads(query.data)
        appointment = get_or_none(Appointment, id=data['id'], status=AppointmentStatus.CREATED)
        if not appointment:
            context.bot.delete_message(
                message_id=query.message.message_id,
                chat_id=query.message.chat_id,
            )
            query.answer('Заявка уже обработанна')
            return
        appointment.status = status
        appointment.save()
        status_result = "подтвержденно" if appointment.status == AppointmentStatus.ACCEPTED else "отклоненно"
        msg = TextMessage.WAIT_FOR_ACCEPT + f'\nДата: {data["date"]}\nВремя: {data["time"]}\n{status_result}'
        context.bot.edit_message_text(
            text=msg,
            message_id=query.message.message_id,
            chat_id=query.message.chat_id,
            reply_markup=None
        )
        msg = f'Доктор: {appointment.doctor.name}\n' \
              f'Статус: {status_result}\n' \
              f'Дата: {appointment.reservation_date.strftime("%Y-%m-%d")}\n' \
              f'Время: {appointment.reservation_date.strftime("%H:%M")}'
        context.bot.send_message(
            text=msg,
            chat_id=appointment.bot_user.chat_id,
        )

    @staticmethod
    def accept_action_pattern():
        return f'^{{"id": \d+, "action": {AppointmentAction.ACCEPT.value}, "date": .+, "time": .+}}$'

    @staticmethod
    def accept(update: Update, context: CallbackContext, status=None):
        AppointmentCallback.appointment_result(update, context, status=AppointmentStatus.ACCEPTED)

    @staticmethod
    def reject_action_pattern():
        return f'^{{"id": \d+, "action": {AppointmentAction.REJECT.value}, "date": .+, "time": .+}}$'

    @staticmethod
    def reject(update: Update, context: CallbackContext):
        AppointmentCallback.appointment_result(update, context, status=AppointmentStatus.REJECT)


def ignore_callback_handler(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()


def str_to_json_format(value):
    return '{{' + value + '}}'


def add_callback_handlers(dispatcher):
    dispatcher.add_handler(
        CallbackQueryHandler(AppointmentCallback.doctor_choice, pattern=AppointmentCallback.doctor_action_pattern())
    )
    dispatcher.add_handler(
        CallbackQueryHandler(AppointmentCallback.date_choice, pattern=AppointmentCallback.date_action_pattern())
    )
    dispatcher.add_handler(
        CallbackQueryHandler(AppointmentCallback.next_date, pattern=AppointmentCallback.next_date_action_pattern())
    )
    dispatcher.add_handler(
        CallbackQueryHandler(AppointmentCallback.prev_date, pattern=AppointmentCallback.prev_date_action_pattern())
    )
    dispatcher.add_handler(
        CallbackQueryHandler(AppointmentCallback.time_choice, pattern=AppointmentCallback.time_action_pattern())
    )
    dispatcher.add_handler(
        CallbackQueryHandler(AppointmentCallback.reject, pattern=AppointmentCallback.reject_action_pattern())
    )
    dispatcher.add_handler(
        CallbackQueryHandler(AppointmentCallback.accept, pattern=AppointmentCallback.accept_action_pattern())
    )
    dispatcher.add_handler(
        CallbackQueryHandler(ignore_callback_handler, pattern='0')
    )
