from telegram.ext import Updater

from bot.callbacks import add_callback_handlers
from bot.handlers import add_handlers


def init_dispatcher(updater: Updater):
    dispatcher = updater.dispatcher
    add_handlers(dispatcher)
    add_callback_handlers(dispatcher)
