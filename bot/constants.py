from enum import Enum, unique


@unique
class ConvState(Enum):
    NAME = 0
    AGE = 1


@unique
class AppointmentAction(Enum):
    DOCTOR = 0
    DATE = 1
    TIME = 2
    DATE_PREV = 3
    DATE_NEXT = 4
    ACCEPT = 5
    REJECT = 6


class TextMessage:
    NAME_TEXT = "Напишите Ф.И.О"
    AGE_TEXT = "Напишите свой возраст"
    AGE_INVALID_TEXT = "Напишите свой возраст (Только числа)"
    SELECT_DOCTOR_TEXT = "Предпочитаемый врач"
    WAIT_FOR_ACCEPT = "Ожидайте подтверждения врача"
    DOCTOR_RESPONSE = 'Ответ врача'


class MenuMessage:
    DOCTORS = 'Доктора'


class InlineMessage:
    ACCEPT = 'подтвердить'
    REJECT = 'отказать'
