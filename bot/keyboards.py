import calendar
import json
import datetime
from django.template.defaultfilters import date as dj_date

from telegram import InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardMarkup

from bot.constants import AppointmentAction, MenuMessage, InlineMessage
from electronic_queue.doctor.models import Doctor


def main_menu():
    reply_keyboard = build_menu(
        buttons=[MenuMessage.DOCTORS],
        n_cols=1,
    )
    kb = ReplyKeyboardMarkup(
        reply_keyboard,
        one_time_keyboard=False,
        resize_keyboard=True
    )
    return kb


def doctor_inline_menu():
    doctors = Doctor.objects.all()
    buttons = []
    for doctor in doctors:
        btn = [InlineKeyboardButton(
            doctor.name,
            callback_data=json.dumps({'id': doctor.id, 'action': AppointmentAction.DOCTOR.value})
        )]
        buttons.append(btn)
    kb = InlineKeyboardMarkup(buttons)
    return kb


def appointment_result_inline_menu(app_id, date, time):
    buttons = []
    for text, action in [(InlineMessage.ACCEPT, AppointmentAction.ACCEPT.value),
                         (InlineMessage.REJECT, AppointmentAction.REJECT.value)]:
        btn = [InlineKeyboardButton(
            text,
            callback_data=json.dumps({
                'id': app_id,
                'action': action,
                "date": date,
                "time": time
            })
        )]
        buttons.append(btn)
    kb = InlineKeyboardMarkup(buttons)
    return kb


def build_menu(buttons,
               n_cols,
               header_buttons=None,
               footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, [header_buttons])
    if footer_buttons:
        menu.append([footer_buttons])
    return menu


def calendar_keyboard(available_days, year=None, month=None, data_ignore=None, callback_data_format='{}',
                      next_format='{}', prev_format='{}'):
    if data_ignore is None:
        data_ignore = '0'
    now = datetime.datetime.now()
    if year is None:
        year = now.year
    if month is None:
        month = now.month
    keyboard = []
    # headers
    row = [InlineKeyboardButton(f'{dj_date(datetime.datetime(year, month, 1), "F Y")}', callback_data=data_ignore)]
    keyboard.append(row)
    row = []
    for day in ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс']:
        row.append(InlineKeyboardButton(day, callback_data=data_ignore))
    keyboard.append(row)
    my_calendar = calendar.monthcalendar(year, month)
    # calendar
    for week in my_calendar:
        row = []
        for day in week:
            if day == 0:
                row.append(InlineKeyboardButton(" ", callback_data=data_ignore))
            else:
                if day in available_days:
                    btn = InlineKeyboardButton(
                        str(day),
                        callback_data=callback_data_format.format(
                            datetime.datetime(year, month, day).strftime('%Y-%m-%d')
                        )
                    )
                    row.append(btn)
                else:
                    row.append(InlineKeyboardButton("✖", callback_data=data_ignore))
        keyboard.append(row)

    # footer
    row = []
    if now.year < year or (now.year == year and now.month < month):
        row.append(InlineKeyboardButton(
            "<",
            callback_data=prev_format.format(datetime.datetime(year, month, 1).strftime('%Y-%m-%d'))
        ))
    else:
        row.append(InlineKeyboardButton(" ", callback_data=data_ignore))
    row.append(InlineKeyboardButton(" ", callback_data=data_ignore))
    row.append(InlineKeyboardButton(
        ">",
        callback_data=next_format.format(datetime.datetime(year, month, 1).strftime('%Y-%m-%d'))
    ))
    keyboard.append(row)

    return InlineKeyboardMarkup(keyboard)


def time_keyboard(times, n_cols=3, data_ignore=None, callback_data_format='{}'):
    if data_ignore is None:
        data_ignore = '0'
    menu = build_menu(times, n_cols)
    kb = []
    for btns in menu:
        row = []
        for btn in btns:
            if btn is None:
                row.append(InlineKeyboardButton("✖", callback_data=data_ignore))
            else:
                row.append(InlineKeyboardButton(btn, callback_data=callback_data_format.format(btn)))
        kb.append(row)
    return InlineKeyboardMarkup(kb)
