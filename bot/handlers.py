from telegram import Update
from telegram.ext import CallbackContext, ConversationHandler, CommandHandler, MessageHandler, Filters

from bot.constants import TextMessage, ConvState, MenuMessage
from bot.keyboards import doctor_inline_menu, main_menu
from electronic_queue.account.models import BotUser


def start(update: Update, context: CallbackContext) -> ConvState:
    update.message.reply_text(
        TextMessage.NAME_TEXT,
    )
    BotUser.objects.update_or_create(
        chat_id=update.message.chat_id,
        defaults=dict(
            is_active=True
        )
    )
    return ConvState.NAME


def user_name(update: Update, context: CallbackContext) -> ConvState:
    text = update.message.text
    context.user_data['name'] = text
    update.message.reply_text(
        TextMessage.AGE_TEXT,
    )
    return ConvState.AGE


def user_age(update: Update, context: CallbackContext) -> ConvState:
    text = update.message.text
    if not text.isnumeric():
        update.message.reply_text(
            TextMessage.AGE_INVALID_TEXT,
        )
        return ConvState.AGE
    bot_user = BotUser.objects.get(chat_id=update.message.chat_id)
    bot_user.name = context.user_data['name']
    bot_user.age = text
    bot_user.save()
    update.message.reply_text(
        bot_user.info_text,
        reply_markup=main_menu()
    )
    doctors(update, context)
    return ConversationHandler.END


def doctors(update: Update, context: CallbackContext):
    update.message.reply_text(
        TextMessage.SELECT_DOCTOR_TEXT,
        reply_markup=doctor_inline_menu()
    )


def add_handlers(dispatcher):
    conv_handler = ConversationHandler(
        entry_points=[
            CommandHandler('start', start),
        ],
        states={
            ConvState.NAME: [
                MessageHandler(Filters.text, user_name),
            ],
            ConvState.AGE: [
                MessageHandler(Filters.text, user_age),
            ],
        },
        fallbacks=[CommandHandler('start', start)],
        allow_reentry=True,
    )
    dispatcher.add_handler(conv_handler)
    dispatcher.add_handler(
        MessageHandler(Filters.regex(f'^{MenuMessage.DOCTORS}$'), doctors)
    )
